package br.com.fabianoss.domain;

import java.io.Serializable;

public class Pessoa implements Serializable{

    /**
     * 
     */
    private static final long serialVersionUID = 1750158109501533915L;
    
    private String lastName;
    private String firstName;

    public Pessoa() {
    }

    public Pessoa(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    @Override
    public String toString() {
	return "Pessoa [lastName=" + lastName + ", firstName=" + firstName + "]";
    }
    
    
    

}
