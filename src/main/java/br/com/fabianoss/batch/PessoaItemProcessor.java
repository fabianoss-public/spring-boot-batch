package br.com.fabianoss.batch;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.batch.item.ItemProcessor;

import br.com.fabianoss.domain.Pessoa;

public class PessoaItemProcessor implements ItemProcessor<Pessoa, Pessoa>{
    
    private static final Logger log = LoggerFactory.getLogger(PessoaItemProcessor.class);
    
    @Override
    public Pessoa process(Pessoa pessoa) throws Exception {
	final String firstName = pessoa.getFirstName().toUpperCase();
        final String lastName = pessoa.getLastName().toUpperCase();

        final Pessoa transformedPessoa = new Pessoa(firstName, lastName);

        log.info("Converting (" + pessoa + ") into (" + transformedPessoa + ")");

        return transformedPessoa;
    }

}
